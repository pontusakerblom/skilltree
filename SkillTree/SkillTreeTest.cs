﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using OpenQA.Selenium;
using SkillTreeProject;
using SkillTreeProject.PageObjectModel;

namespace SkillTreeProject
{
    [TestClass]
    public class SkillTreeTest : TestBase
    {
        
        [TestMethod]
        [TestCategory("Working flow")]

        public void DAT_Test()
        {
            new SkillTree(driver)

                .FillEmail(driver, "pontus.akerblom@sogeti.com")
                .YP_Grade(driver)
                .Agile_Grade_A(driver)
                .Agile_Grade_B(driver)
                .Agile_Grade_C(driver)
                .DevOps_Grade_A(driver)
                .DevOps_Grade_B(driver)
                .DevOps_Grade_C(driver)
                .Automation_Grade_A(driver)
                .Automation_Grade_B(driver)
                .Automation_Grade_C(driver)
                .Performance_Grade_A(driver)
                .Performance_Grade_B(driver)
                .Performance_Grade_C(driver);
               

                }
    }
}
