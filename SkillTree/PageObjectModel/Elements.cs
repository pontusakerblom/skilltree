﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.IO;
using static System.Net.Mime.MediaTypeNames;

namespace SkillTreeProject.PageObjectModel
{
    public static class MyVariables
    {
        private static string jsonValues = File
            .ReadAllText(@"C:\Users\pakerblo\source\repos\SkillTree\SkillTree\Locales\locations.json", System.Text
            .Encoding.Default);
        /*  private static string jsonValues = File
              .ReadAllText(Path.Combine(System.Environment.CurrentDirectory), "\\SvEn.json"); */

                public static Elements GetValues() { 
        
                Elements myVariables = JsonConvert.DeserializeObject<Elements>(jsonValues);
                return myVariables;
        }
    }
    public class Elements
    {
        public string LogIn_Form { get; set; }
        public string LogIn_NextButton { get; set; }
        public string DA_Testing_Function_Link { get; set; }
        public string YP_Grade_Link { get; set; }
        public string Agile_Grade_Link { get; set; }
        public string Agile_A_Button { get; set; }
        public string Agile_B_Button { get; set; }
        public string Agile_C_Button { get; set; }
        public string DevOps_Grade_Link { get; set; }
        public string DevOps_A_Button { get; set; }
        public string DevOps_B_Button { get; set; }
        public string DevOps_C_Button { get; set; }
        public string Automation_Grade_Link { get; set; }
        public string QA_Automation_A_Button { get; set; }
        public string QA_Automation_B_Button { get; set; }
        public string QA_Automation_C_Button { get; set; }
        public string Performance_Grade_Link { get; set; }
        public string Performance_A_Button { get; set; }
        public string Performance_B_Button { get; set; }
        public string Performance_C_Button { get; set; }
        public string Sharepoint_Button { get; set; }
        public string Sogeti_Logo { get; set; }
        public string Home_Button { get; set; }
      

    }
}
