﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using System;
using System.Collections;
using System.Text.RegularExpressions;
using SkillTreeProject.PageObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System.Linq;
using OpenQA.Selenium.Interactions;
using AutoIt;
using System.Linq.Expressions;
using System.Collections.Generic;
using HtmlAgilityPack;
using System.Net.Http;  
using System.IO;
using System.Text;
using OpenQA.Selenium.Support.UI;

namespace SkillTreeProject
{
    public class SkillTree : TestBase
    {
        
        Elements elementer = MyVariables.GetValues();



        public SkillTree(IWebDriver driver)
        {
            this.driver = driver;

        }

        public bool isAlertPresent()
        {

            bool presentFlag = false;

            try
            {

                // Check the presence of alert
                IAlert alert = driver.SwitchTo().Alert();
                // Alert present; set the flag
                presentFlag = true;
                // if present consume the alert
                alert.Accept();

            }
            catch (NoAlertPresentException ex)
            {
                // Alert not present
                //Console.WriteLine(ex.ToString());
            }

            return presentFlag;

        }


        public SkillTree FillEmail(IWebDriver driver, string email)
        {
            driver.FindElement(By.XPath(elementer.LogIn_Form))
                .SendKeys(email);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.LogIn_NextButton))
                .Click();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);



            return this;

        }

        public SkillTree YP_Grade(IWebDriver driver)
        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.YP_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("YOUNG PROFESSIONAL");
            Console.WriteLine("");
            Console.WriteLine("");

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                isAlertPresent();

                var loading_page = driver.Title;
                // The title of the loading page has to be stored first

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                var page_title = driver.Title;
                // The actual title of the page


                try
                {

                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


                
            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }
        public SkillTree Agile_Grade_A(IWebDriver driver)

        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Agile_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);
           
            driver.FindElement(By.XPath(elementer.Agile_A_Button))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("AGILE A");
            Console.WriteLine("");
            Console.WriteLine("");


            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                isAlertPresent();

                var loading_page = driver.Title;

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                var page_title = driver.Title;

                try
                {
                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }



        public SkillTree Agile_Grade_B(IWebDriver driver)
        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Agile_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Agile_B_Button))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("AGILE B");
            Console.WriteLine("");
            Console.WriteLine("");


            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                isAlertPresent();

                var loading_page = driver.Title;

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                var page_title = driver.Title;

                try
                {
                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }

        public SkillTree Agile_Grade_C(IWebDriver driver)
        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Agile_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Agile_C_Button))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("AGILE C");
            Console.WriteLine("");
            Console.WriteLine("");


            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                isAlertPresent();

                var loading_page = driver.Title;

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                var page_title = driver.Title;

                try
                {
                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }

        public SkillTree DevOps_Grade_A(IWebDriver driver)
        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DevOps_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DevOps_A_Button))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("DEVOPS A");
            Console.WriteLine("");
            Console.WriteLine("");


            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                isAlertPresent();

                var loading_page = driver.Title;

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                var page_title = driver.Title;

                try
                {
                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }
        public SkillTree DevOps_Grade_B(IWebDriver driver)
        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DevOps_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DevOps_B_Button))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("DEVOPS B");
            Console.WriteLine("");
            Console.WriteLine("");


            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                isAlertPresent();

                var loading_page = driver.Title;

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                var page_title = driver.Title;

                try
                {
                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }

        public SkillTree DevOps_Grade_C(IWebDriver driver)
        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DevOps_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DevOps_C_Button))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("DEVOPS C");
            Console.WriteLine("");
            Console.WriteLine("");


            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                isAlertPresent();

                var loading_page = driver.Title;

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                var page_title = driver.Title;

                try
                {
                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }

        public SkillTree Automation_Grade_A(IWebDriver driver)
        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Automation_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.QA_Automation_A_Button))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("AUTOMATION A");
            Console.WriteLine("");
            Console.WriteLine("");


            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                isAlertPresent();

                var loading_page = driver.Title;

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(20);

                var page_title = driver.Title;

                try
                {
                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }

        public SkillTree Automation_Grade_B(IWebDriver driver)
        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Automation_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.QA_Automation_B_Button))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("AUTOMATION B");
            Console.WriteLine("");
            Console.WriteLine("");


            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(20);

                isAlertPresent();

                var loading_page = driver.Title;

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                var page_title = driver.Title;

                try
                {
                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }

        public SkillTree Automation_Grade_C(IWebDriver driver)
        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Automation_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.QA_Automation_C_Button))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("AUTOMATION C");
            Console.WriteLine("");
            Console.WriteLine("");


            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                isAlertPresent();

                var loading_page = driver.Title;

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                var page_title = driver.Title;

                try
                {
                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }

        public SkillTree Performance_Grade_A(IWebDriver driver)
        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Performance_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Performance_A_Button))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("PERFORMANCE A");
            Console.WriteLine("");
            Console.WriteLine("");


            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                isAlertPresent();

                var loading_page = driver.Title;

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(20);

                var page_title = driver.Title;

                try
                {
                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }

        public SkillTree Performance_Grade_B(IWebDriver driver)
        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Performance_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Performance_B_Button))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("PERFORMANCE B");
            Console.WriteLine("");
            Console.WriteLine("");


            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                isAlertPresent();

                var loading_page = driver.Title;

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(20);

                var page_title = driver.Title;

                try
                {
                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }

        public SkillTree Performance_Grade_C(IWebDriver driver)
        {

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.DA_Testing_Function_Link))
               .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Performance_Grade_Link))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            driver.FindElement(By.XPath(elementer.Performance_C_Button))
                .Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            Console.WriteLine("PERFORMANCE C");
            Console.WriteLine("");
            Console.WriteLine("");


            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

            IWebElement tableElement = driver.FindElement(By.XPath("//table[@id='skills-table']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath(".//a"));


            foreach (IWebElement element in tableRow)
            {


                var link_text = element.Text;

                element.Click();
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);


                driver.SwitchTo().Window(driver.WindowHandles.Last());

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                isAlertPresent();

                var loading_page = driver.Title;

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(50);

                var page_title = driver.Title;

                try
                {
                    Assert.IsTrue(page_title.Contains(link_text));

                }

                catch (Exception)
                {

                    Console.WriteLine("EXPECTED: " + link_text + "   " + " ACTUAL: " + page_title);
                    Console.WriteLine("");
                    Console.WriteLine("");
                }



                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());


            }

            driver.Navigate().GoToUrl("https://capgemini.sharepoint.com/sites/SogetiSE-SCF/");

            return this;

        }
    }
    }



    

    
                


            



        


    



          
    



    


