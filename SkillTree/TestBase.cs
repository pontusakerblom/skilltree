﻿using SkillTreeProject.PageObjectModel;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Threading;

namespace SkillTreeProject
{
    
    public class TestBase
    {
        public TestContext testContextInstance;
        public IWebDriver driver;
        public string URL;


        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void SetupTest()
        {

            URL = "https://capgemini.sharepoint.com/sites/SogetiSE-SCF/";

            string browser = "Chrome";
            switch (browser)
            {
                case "Chrome":
                    driver = new ChromeDriver();
                    break;
                case "Firefox":
                    driver = new FirefoxDriver();
                    break;
                case "IE":
                    driver = new InternetExplorerDriver();
                    break;
                default:
                    driver = new ChromeDriver();
                    break;
            }
            driver.Navigate().GoToUrl(URL);
            driver.Manage().Window.Maximize();
        }
        [TestCleanup()]
        public void MyTestCleanup()
        {
            driver.Close();
            driver.Quit();
        }
    }
}